#include "JudgeManager.h"
#include "SqliteHelper.h"
#include "CharacterEntity.h"
#include "SQLiteData.h"
#include "DataTool.h"

#include <algorithm>
using namespace std;


JudgeManager::JudgeManager()
{

}

JudgeManager::~JudgeManager()
{
}

string JudgeManager::getResult(string hanzi,string points_output,CharacterEntity* p, string funcs){

	string filepath = "lua/WriteZiInfo.lua";
	string basepath = "lua/BaseLib.lua";
	string apipath = "lua/RunAPI.lua";
//    string standard = "lua_ext/StandardZiInfo.lua";

	CLuaScriptReader gReader;

	gReader.InitLuaScriptReader();
	gReader.setWriteZiInfo(points_output.c_str());
 	char * retStr = new char[50];
 	gReader.setZiName(hanzi);

    string luas = p->getRules();
//    gReader.setLevel("1");
//  	gReader.setRulesFunc(luas);
    gReader.setRulesFunc(luas);
    
//    gReader.setStandardZiInfo("40/221/0/433/221/0/@");


    const char* writeziinfo = "WriteZiInfo.lua";
	gReader.RunScriptFile(filepath.c_str(),writeziinfo);
//    gReader.RunScriptFile(standard.c_str(),"StandardZiInfo.lua");

	gReader.setGlobalFunc(funcs);
    const char* baselua = "BaseLib.lua";
    gReader.RunScriptFile(baselua, "BaseLib.lua");
    // 	gReader.RunMixedFile(basepath.c_str(),baselua);
    const char* runapilua = "RunAPI.lua";
 	gReader.RunScriptFile(apipath.c_str(),retStr,runapilua);
 	gReader.ExitLuaScriptReader();
	string ret(retStr);
	delete [] retStr;
	return ret;

}

string JudgeManager::getResult(string hanzi , string points_output, string all_points, CharacterExtend* p , string funcs){

	string filepath = "lua_ext/WriteZiInfo.lua";
	string basepath = "lua_ext/BaseLib.lua";
	string apipath = "lua_ext/RunAPI.lua";
	string standardpath = "lua_ext/StandardZiInfo.lua";

	CLuaScriptReader gReader;

	gReader.InitLuaScriptReader();
	gReader.setWriteZiInfo(points_output.c_str());
	gReader.setStandardZiInfo(all_points);
/*	char * retStr = new char[50];*/
	char retStr[50];
	retStr[0] = '\0';
 	gReader.setZiName(hanzi);

	// get easy or hard setting
    string r = "1";
 	gReader.setLevel(r);
 	if (r.compare("1") == 0)
 	{
        std::cout << "loose rule" << endl;
        string looselua = p->getRuleLoose();
 		gReader.setRulesFunc(looselua);
 	}else if(r.compare("2") == 0){
        string tightlua = p->getRuleTight();
 		gReader.setRulesFunc(tightlua);
 	}

// 	DataTool::storeToFile(funcs.c_str(),"func.txt");
//	CCLog("setGlobalFunc %s",funcs.c_str());
//	gReader.setGlobalFunc(funcs);

 	gReader.RunScriptFile(filepath.c_str(),"WriteZiInfo.lua");
 	gReader.RunScriptFile(standardpath.c_str(),"StandardZiInfo.lua");

// 	gReader.RunMixedFile(basepath.c_str(),"BaseLib.lua");
 	gReader.RunScriptFile(basepath.c_str(),"BaseLib.lua");

	gReader.RunScriptFile(apipath.c_str(),retStr,"RunAPI.lua");
//	gReader.RunScriptFile(apipath.c_str(),"RunAPI.lua");

	gReader.ExitLuaScriptReader();

    string ret = retStr;
	return ret;
//	return string("1\r\n");
}
