//
//  main.cpp
//  test
//
//  Created by Ein Verne on 15/10/4.
//  Copyright (c) 2015年 Ein Verne. All rights reserved.
//

#include <boost/python.hpp>
#include <iostream>
#include <string>
#include "JudgeManager.h"
#include "CharacterExtend.h"
#include "SQLiteData.h"

using namespace boost::python;
using namespace std;

//int main(int argc, const char * argv[]) {
//    // some paramaters
//    // c stand for currentCharacter
//    
//    // points stand for points_output
//    
//    //
//    
//    std::string currentCharacter = "一";
//    std::string points_out = "69/246/124/244/247/244/378/244/417/237/417/237/@";
//    // 90/267/92/267/124/267/157/267/209/267/297/267/320/267/333/267/358/267/371/267/385/267/401/267/423/267/435/267/441/267/446/267/448/264/448/264/@
//    std::string all_points = "40/221/0/433/221/0/@";
//    CharacterEntity* p = new CharacterEntity();
//    SQLiteData::getHanziData(currentCharacter, p);
//    std::string funcs="";
//    std::string ret = JudgeManager::getResult(currentCharacter, points_out, p, funcs);
//    std::cout << "ret value: " << ret.c_str() << endl;
//    delete p;
//    
//    return 0;
//}

char const* greet(){
    return "hello world";
}

BOOST_PYTHON_MODULE(libjudge){
    //export a hello function
    using namespace boost::python;
    def("greet", greet);
};

