#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "Bujian.h"
#include <vector>
#include <string>
using namespace std;

class Character
{
public:
	bool addBujian(Bujian bujian);

	/**
	* getStrokeCount 获取字的笔画数
	* @return
	*/
	int getStrokeCount();
	void resample();
    
	Character(void);
	~Character(void);
	int bujianCount;
	int fontSize;		//汉字大小，默认512
	vector<Bujian> bujianList;
    Stroke getStroke(int no);
    string getCharacterStandardInfo();
private:
	void transformCoordinate(CCPoint point,float length);

};

#endif