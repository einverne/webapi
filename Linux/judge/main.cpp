//
//  main.cpp
//  test
//
//  Created by Ein Verne on 15/10/4.
//  Copyright (c) 2015年 Ein Verne. All rights reserved.
//

#include <iostream>
#include <string>

#include "JudgeManager.h"
#include "CharacterExtend.h"
#include "SQLiteData.h"
#include "ReadXML.h"
#include "Character.h"
#include <boost/python.hpp>
using namespace boost::python;

string getret(string curChar, string points_out){
    // some paramaters
    // c stand for currentCharacter
    
    // points stand for points_output
    
    //90/267/92/267/124/267/157/267/209/267/297/267/320/267/333/267/358/267/371/267/385/267/401/267/423/267/435/267/441/267/446/267/448/264/448/264/@
    
    //string all_points = "251/62/0/236/138/0/223/196/0/200/262/0/170/324/0/125/380/0/69/415/0/30/432/0/@225/183/0/253/257/0/278/300/0/320/353/0/366/393/0/435/438/0/@";

	if(curChar.empty() || points_out.empty()) return 0;

    CharacterExtend* p = new CharacterExtend();
    if(!p) return 0;
    SQLiteData::getHanziDataExtend(curChar, p);
    CReadXML xmlparse(p->getXML().c_str());
    cout << "p name" << p->getName() << endl;
    Character ch = xmlparse.getCharacter();

    string all_points = ch.getCharacterStandardInfo();
    cout << all_points << endl;


    string funcs="";
    string ret = JudgeManager::getResult(curChar, points_out, all_points, p, funcs);
    std::cout << "ret value: " << ret << endl;
    delete p;
    return ret;
}

int main(int argc, const char * argv[]) {
    //string curChar = "一";
    //string points_out = "69/246/124/244/247/244/378/244/417/237/417/237/@";

    string curChar = "人";
    string points_out = "282/105/276/123/273/130/269/142/262/163/249/193/244/201/238/208/236/219/229/232/223/241/214/247/188/276/181/286/172/290/140/307/133/313/128/317/112/327/104/330/91/337/84/341/81/343/78/346/68/348/66/350/59/352/55/355/55/355/@";
    // 人 282/105/276/123/273/130/269/142/262/163/249/193/244/201/238/208/236/219/229/232/223/241/214/247/188/276/181/286/172/290/140/307/133/313/128/317/112/327/104/330/91/337/84/341/81/343/78/346/68/348/66/350/59/352/55/355/55/355/@
    // all 251/62/0/236/138/0/223/196/0/200/262/0/170/324/0/125/380/0/69/415/0/30/432/0/@225/183/0/253/257/0/278/300/0/320/353/0/366/393/0/435/438/0/@
    
    string ret = getret(curChar, points_out);
    return 0;
}


char const* greet(){
    return "hello world";
}

string greeting(){
    return "hello string";
}

BOOST_PYTHON_MODULE(libjudge){
    //export a hello function
    using namespace boost::python;
    def("greet", greet);
    def("hellogreeting", greeting);
    def("getret", getret, args("curChar", "points_out"), "get return result");
};
