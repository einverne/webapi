#include "Character.h"


Character::Character(void)
{
	bujianCount = 0;
	fontSize = 512;
}

Character::~Character(void)
{
	bujianList.clear();
}

bool Character::addBujian(Bujian bujian){
	
	vector<Bujian>::iterator it = bujianList.end();
	bujianList.insert(it,bujian);
	bujianCount++;
	return true;
}


//////////////////////////////////////////////////////////////////////////
// 改变坐标系，将读取的xml坐标系做转换，符合cocos2d-x第一象限坐标系
//////////////////////////////////////////////////////////////////////////
void Character::transformCoordinate(CCPoint point,float length){

    for (int bujiani = 0 ; bujiani < bujianCount ; ++ bujiani)
	{
		Bujian bujian = bujianList.at(bujiani);//(Bujian)*iter;
		vector<Stroke> strokeList = bujian.strokeList;
		for (int strokei = 0; strokei < bujian.strokeCount; ++strokei)
		{
			Stroke stroke = strokeList.at(strokei);//(Stroke)*stro_iter;
			vector<CCPoint> pointList = stroke.getpointList();
			for (int i = 0;i < stroke.getPointsCount(); i++)
			{
				CCPoint temppoint = pointList.at(i);
				//temppoint = temppoint - point;
				temppoint.y = - temppoint.y;
				//temppoint.y = temppoint.y + length;
				temppoint.y += fontSize;
				///////////////////////////////////////////////
// 				bujianList[bujiani].strokeList[strokei].pointList.erase(pointList.begin()+i);
// 				vector<CCPoint>::iterator po_iter = bujianList[bujiani].strokeList[strokei].pointList.begin();
// 				bujianList[bujiani].strokeList[strokei].pointList.insert(po_iter+i,ccp(temppoint.x,temppoint.y));
				bujianList[bujiani].strokeList[strokei].setpointList(i,CCPoint(temppoint.x,temppoint.y));
				///////////////////////////////////////////////
				//pointList.erase(pointList.begin()+i);
				//vector<CCPoint>::iterator po_iter = pointList.begin();
				//pointList.insert(po_iter+i,ccp(temppoint.x,temppoint.y));		//坐标转换
				//pointList[i] = ccp(temppoint.x,temppoint.y);
			}
		}
	}
}

int Character::getStrokeCount(){
	int count = 0;
	for (vector<Bujian>::iterator iter = bujianList.begin(); iter != bujianList.end(); ++ iter)
	{
		count += ((Bujian)*iter).strokeCount;
	}
	return count;
}

//重采样
void Character::resample(){
	for (int bujiani = 0 ; bujiani < bujianCount ; ++ bujiani)
	{
		Bujian bujian  = bujianList.at(bujiani);
		for (int strokei = 0 ; strokei < bujian.strokeCount ; ++ strokei)
		{
			bujianList[bujiani].strokeList[strokei].resample();
		}
	}
}

Stroke Character::getStroke(int no){
    int totalStrokeCount = 0;
    int tag = 0;
    Stroke retStroke;
    for (int i = 0; i < this->bujianCount ; ++i)
    {
        totalStrokeCount += bujianList[i].strokeCount;
    }
    if (no <= totalStrokeCount)
    {
        //小于全部笔画数
        for (int j = 0 ; j < this->bujianList.size() ; ++j )
        {
            Bujian bujian_temp = bujianList[j];
            for (int k = 0 ; k < bujian_temp.strokeList.size() ; ++k)
            {
                tag++;
                if (tag == no)
                {
                    retStroke = bujian_temp.strokeList[k];
                }
            }
        }
    }
    return retStroke;
}

string Character::getCharacterStandardInfo(){
    string ret = "";
    int cout = getStrokeCount();
    for (int i = 1 ; i <= cout ; i++)
    {
        Stroke stroke = getStroke(i);
        ret += stroke.sendOutputWithStatus();
    }
    return ret;
}