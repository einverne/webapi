//
//  CCPoint.cpp
//  test
//
//  Created by Ein Verne on 15/10/30.
//  Copyright © 2015年 Ein Verne. All rights reserved.
//

#include "CCPoint.hpp"

CCPoint::CCPoint(void) : x(0), y(0)
{
}

CCPoint::CCPoint(float x, float y) : x(x), y(y)
{
}

CCPoint::CCPoint(const CCPoint& other) : x(other.x), y(other.y)
{
}

CCPoint& CCPoint::operator= (const CCPoint& other)
{
    setPoint(other.x, other.y);
    return *this;
}

CCPoint CCPoint::operator+(const CCPoint& right) const
{
    return CCPoint(this->x + right.x, this->y + right.y);
}

CCPoint CCPoint::operator-(const CCPoint& right) const
{
    return CCPoint(this->x - right.x, this->y - right.y);
}

CCPoint CCPoint::operator-() const
{
    return CCPoint(-x, -y);
}

CCPoint CCPoint::operator*(float a) const
{
    return CCPoint(this->x * a, this->y * a);
}

CCPoint CCPoint::operator/(float a) const
{
    return CCPoint(this->x / a, this->y / a);
}

void CCPoint::setPoint(float x, float y)
{
    this->x = x;
    this->y = y;
}

bool CCPoint::fuzzyEquals(const CCPoint& b, float var) const
{
    if(x - var <= b.x && b.x <= x + var)
        if(y - var <= b.y && b.y <= y + var)
            return true;
    return false;
}
