## Webapi doc

python web 基于 webpy.org 实现 get post 请求处理。

###  运行

执行命令

    python index.py

### 关于 HTTP 请求说明

GET请求参数为：

http://localhost:8080/judge?c=人&points=pointslist

- c  参数代表需评判汉字，比如例子中为评判“人”字

- points 参数代表传入的当前笔迹信息，包含了当前一笔以及之前所有写过的笔迹。

	格式说明如下：

		x/y/x/y/@x/y/x/y/@

	横坐标点与纵坐标点之间以 / 分割，点与点之间也用 / 分割，而笔画与笔画之间使用 @ 号分割

	评判使用的坐标系为第四象限坐标系，横轴从左到右为0~512，纵轴从上到下为0~512.  所有笔画在该正方形区域中。

POST 请求参数：

同上

### 关于 so 文件使用

so 文件为评判引擎库，供 python 调用， 动态链接库中暴露给 python 的接口函数为

    string getret(string curChar, string points_out)

同上，该接口函数需要两个参数，待评价的汉字，以及点序列。


###  待解决问题

1. 如果请求数据过多可能造成的问题。
2. 并行读取数据可能存在的问题。SQLite 的运行性能是否能够支持网站的并发请求。
3. 通过 Python 方式间接实现访问 C++ 工程的可能，中间效率问题是否能够优化。

所以如果要实现 Web 版需要考虑的问题还是挺多。
