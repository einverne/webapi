################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Bujian.cpp \
../CCPoint.cpp \
../Character.cpp \
../CharacterEntity.cpp \
../CharacterExtend.cpp \
../DataTool.cpp \
../JudgeManager.cpp \
../LuaScriptReader.cpp \
../ReadXML.cpp \
../SQLiteData.cpp \
../SqliteHelper.cpp \
../Stroke.cpp \
../main.cpp \
../tinystr.cpp \
../tinyxml.cpp \
../tinyxml2.cpp \
../tinyxmlerror.cpp \
../tinyxmlparser.cpp 

C_SRCS += \
../sqlite3.c 

OBJS += \
./Bujian.o \
./CCPoint.o \
./Character.o \
./CharacterEntity.o \
./CharacterExtend.o \
./DataTool.o \
./JudgeManager.o \
./LuaScriptReader.o \
./ReadXML.o \
./SQLiteData.o \
./SqliteHelper.o \
./Stroke.o \
./main.o \
./sqlite3.o \
./tinystr.o \
./tinyxml.o \
./tinyxml2.o \
./tinyxmlerror.o \
./tinyxmlparser.o 

C_DEPS += \
./sqlite3.d 

CPP_DEPS += \
./Bujian.d \
./CCPoint.d \
./Character.d \
./CharacterEntity.d \
./CharacterExtend.d \
./DataTool.d \
./JudgeManager.d \
./LuaScriptReader.d \
./ReadXML.d \
./SQLiteData.d \
./SqliteHelper.d \
./Stroke.d \
./main.d \
./tinystr.d \
./tinyxml.d \
./tinyxml2.d \
./tinyxmlerror.d \
./tinyxmlparser.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/local/include -I/usr/include/lua5.1 -I/usr/include/python2.7 -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -lm -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


