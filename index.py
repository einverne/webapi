#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import web
import libjudge

urls = (
        '/judge(.*)','Judge'
    )

app = web.application(urls, globals())

web.config.debug = True

class Judge:

    def GET(self, name= None):
        user_data = web.input()
        if 'c' not in user_data or 'points' not in user_data:
            return "need para c and points"
        c = user_data.c.encode("utf-8")
        if not c:
            return "c is empty"
        points = user_data.points.encode("utf-8")
        if not points:
            return "points is empty"
        print(c+points)
        judge = libjudge.getret(c, points)
        libjudge.greet()
        return judge

    def POST(self, name):
        return "post"

if __name__=='__main__':
    app = web.application(urls, globals())
    app.run()
