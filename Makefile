# lua c++ for web api

# location of the Python header files

PYTHON_VERSION = 2.7
PYTHON_INCLUDE = /usr/include/python$(PYTHON_VERSION)
  
# location of the Boost Python include files and library
#  
BOOST_INC = /usr/local/include
BOOST_LIB = /usr/local/lib
PYTHON_INC = /System/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7
SQLITE_INC = /usr/include/
LUA_LIB = ~/cocos2d-x-2.2.6/scripting/lua/
#   
# compile mesh classes

CC=g++
CFLAGS=-c -Wall
LDFLAGS=
SOURCES=main.cpp CharacterEntity.cpp CharacterExtend.cpp DataTool.cpp SqliteHelper.cpp SQLiteData.cpp JudgeManager.cpp LuaScriptReader.cpp
OBJECTS=$(SOURCES:.cpp=.o)


TARGET = judge
 
$(TARGET).so: $(OBJECTS)
	$(CC) -shared -Wl $(OBJECTS) -L$(BOOST_LIB) -llua -lboost_python -L/usr/lib/python2.7/config -lpython2.7 -lsqlite3 -o $(TARGET).so
	 
#main.o: main.cpp
	#$(CC) -I$(PYTHON_INCLUDE) -I$(BOOST_INC) -I$(PYTHON_INC) -I$(LUA_INC) -fPIC -c main.cpp

%.o: %.cpp
	$(CC) -I$(PYTHON_INCLUDE) -I$(BOOST_INC) -I$(PYTHON_INC) -I$(SQLITE_INC) -c -o $@ $<

clean:
	@echo "clean project"
	rm *.o *.so
	@echo "clean complete"

