//
//  boost_test.cpp
//  test
//
//  Created by Ein Verne on 15/10/23.
//  Copyright © 2015年 Ein Verne. All rights reserved.
//

#include "boost_test.hpp"
#include <boost/python.hpp>
using namespace boost::python;


char const* greet(){
    return "hello world";
}

BOOST_PYTHON_MODULE(hello){
    //export a hello function
    using namespace boost::python;
    def("greet", greet);
};