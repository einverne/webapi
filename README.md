## Webapi doc

python web 基于 webpy.org 实现 get post 请求处理。

###  运行

执行命令

    python index.py

### 关于 HTTP 请求说明

GET请求参数为：

http://localhost:8080/judge?c=人&points=pointslist

- c  参数代表需评判汉字，比如例子中为评判“人”字

- points 参数代表传入的当前笔迹信息，包含了当前一笔以及之前所有写过的笔迹。

	格式说明如下：

		x/y/x/y/@x/y/x/y/@

	横坐标点与纵坐标点之间以 / 分割，点与点之间也用 / 分割，而笔画与笔画之间使用 @ 号分割

	评判使用的坐标系为第四象限坐标系，横轴从左到右为0~512，纵轴从上到下为0~512.  所有笔画在该正方形区域中。

POST 请求参数：

同上

### 关于 so 文件使用

so 文件为评判引擎库，供 python 调用， 动态链接库中暴露给 python 的接口函数为

    string getret(string curChar, string points_out)

同上，该接口函数需要两个参数，待评价的汉字，以及点序列。


###  待解决问题

1. 如果请求数据过多可能造成的问题。
2. 并行读取数据可能存在的问题。SQLite 的运行性能是否能够支持网站的并发请求。
3. 通过 Python 方式间接实现访问 C++ 工程的可能，中间效率问题是否能够优化。

所以如果要实现 Web 版需要考虑的问题还是挺多。


## Linux 下

## Lua

我们使用的版本是 5.1

    sudo apt install liblua5.1-0-dev

头文件地址 `/usr/include/lua5.1/` , 库文件地址 `/usr/lib/x86_64-linux-gnu/liblua5.1.so`.

查看本机位置

    dpkg -L liblua5.1-0-div

需要： `/usr/lib/x86_64-linux-gnu/liblua5.1.so`

在 gcc 链接的时候链接 llua5.1

add link command `-llua5.1`

## boost python

apt-get install libboost-all-dev

apt-get install libboost-python1.54-dev

库文件地址 `/usr/lib/x86_64-linux-gnu/libboost_python.so`。

## python

apt-get install libpython2.7 libpython2.7-dev

库文件地址 `/usr/lib/x86_64-linux-gnu/libpython2.7.so`。

## sqlite3

sudo apt install libsqlite3-dev


服务端部署

因为使用 Web.py , 看教程选择了使用 [gunicorn](http://gunicorn.org/#quickstart) 来部署，参考[猛禽](http://blog.csdn.net/raptor/article/details/8681357) 的文章。

pip install gunicorn

在 web.py 的文件例如 `index.py` 中加入下面 `application = app.wsgifunc()` 一行，使之成为 wsgi 应用。

    #  ...
    app = web.application(urls, globals())
    #  在这里加入下面这句，即可
    application = app.wsgifunc()

简单的运行 `gunicorn index:application` 。 index 就是文件名， application 就是wsgifunc的名字。

默认监听 127.0.0.1:8000 web server。如需通过网络访问，绑定不同地址和端口：

	gunicorn -b ip:4444 index:application

这样通过 ip + 端口就能访问。另外还有开启多线程的方式可以参考官网地址或者其他文章。另外反向代理可以参考其他教程 Nginx 和 Apache 的反向代理都只需要修改配置文件即可。


## Example

web.py code filename `index.py`:

	#!/usr/bin/python
	# -*- coding: utf-8 -*-

	import web 
	urls = ("/.*", "hello")
	class hello:
		def GET(self):
			return 'Hello, world!'
	app = web.application(urls, globals())
	application = app.wsgifunc()

run as:

	gunicorn -b ip:port index:application&

and then everythin is OK. You can visit `ip:port` to check.
